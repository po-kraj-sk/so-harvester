#!/usr/bin/python3
#
# Copyright 2019 by Peter Hanecak <hanecak+wb@hany.sk>
# Copyright 2017,2019 by Miguel Expósito Martín <https://twitter.com/predicador37>
#
# Licensed under the EUPL, Version 1.2 or - as soon they will be approved
# by the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at:
#
# https://joinup.ec.europa.eu/software/page/eupl 5
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing permissions
# and limitations under the Licence.
#
#
# This harvests and keeps up-to-date statistical data
# from Statistical Office (SO) in local PostgreeSQL database.
#
# Main points:
# a) on first run creates tables in empty DB and gets all data from SO
# b) on subsequent runs gets only updates
# c) one cube -> one DB table

import configparser
import datetime
import logging
import os
import re
import requests
import requests_cache
import sys
import time
import traceback

from argparse import ArgumentParser
from logging.config import fileConfig

from prometheus_client import CollectorRegistry, Gauge, Summary, push_to_gateway

from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry

from sqlalchemy import create_engine, inspect
from sqlalchemy import Column, ForeignKey, MetaData, Table, UniqueConstraint
from sqlalchemy import Date, Float, Integer, String
from sqlalchemy.dialects.postgresql import insert
from sqlalchemy.orm import declarative_base, registry, relationship, sessionmaker
from sqlalchemy.orm.exc import NoResultFound


fileConfig("logging_config.ini")
logger = logging.getLogger()


class SOHConfig:

    CONFIG_FILE_NAME = "so_harvester.conf"
    CONFIG_SECTION_HARVESTER = "so_harvester"
    CONFIG_SECTION_PROMETHEUS = "prometheus"
    CONFIG_SECTION_REQUESTS = "requests"
    CONFIG_SECTION_SQLALCHEMY = "sqlalchemy"

    def __init__(self):
        # monkey-patch -> ugly, but env. variables are case insensitive and thus with default
        # behavior of ConfigParser may trigger duplicates exception
        configparser.ConfigParser.optionxform = str
        cfg_parser = configparser.ConfigParser(os.environ)
        cfg_parser.read(self.CONFIG_FILE_NAME)

        try:
            # harvester - mandatory:
            self.cube_list = cfg_parser.get(self.CONFIG_SECTION_HARVESTER, "cube_list")
            self.db_string = cfg_parser.get(self.CONFIG_SECTION_HARVESTER, "db")

            # harvester - optional:
            self.be_careful = cfg_parser.getboolean(
                self.CONFIG_SECTION_HARVESTER, "be_careful", fallback=True
            )
            self.cache_expire = cfg_parser.getint(
                self.CONFIG_SECTION_HARVESTER, "cache_expire", fallback=86400
            )
            self.contact_email = cfg_parser.get(
                self.CONFIG_SECTION_HARVESTER, "contact_email", fallback=None
            )
            self.db_schema = cfg_parser.get(
                self.CONFIG_SECTION_HARVESTER, "db_schema", fallback="statsoffice"
            )
            self.filter_locations = cfg_parser.get(
                self.CONFIG_SECTION_HARVESTER, "filter_locations", fallback=None
            )
            self.index_columns = cfg_parser.get(
                self.CONFIG_SECTION_HARVESTER, "index_columns", fallback=None
            )
            self.throttle_sleep = cfg_parser.getfloat(
                self.CONFIG_SECTION_HARVESTER, "throttle_sleep", fallback=0.25
            )

            # requests library - optional:
            self.retry_connect = cfg_parser.getint(
                self.CONFIG_SECTION_REQUESTS, "retry_connect", fallback=3
            )
            self.retry_backoff = cfg_parser.getfloat(
                self.CONFIG_SECTION_REQUESTS, "retry_backoff", fallback=0.25
            )

            # Prometheus monitoring - optional:
            self.prometheus_uri = cfg_parser.get(
                self.CONFIG_SECTION_PROMETHEUS, "uri", fallback=None
            )

            # SQLAlchemy - optional:
            self.sqlalchemy_echo = cfg_parser.getboolean(
                self.CONFIG_SECTION_SQLALCHEMY, "echo", fallback=False
            )
        except (configparser.NoOptionError, configparser.NoSectionError) as e:
            logger.error("%s (file %s)" % (e, self.CONFIG_FILE_NAME))
            sys.exit(-1)


config = SOHConfig()


# Prometheus metrics
prom_registry = CollectorRegistry()

prom_harvest_time = Summary(
    "request_processing_seconds",
    "Time spent processing request",
    registry=prom_registry,
)
prom_last_harvest_time = Gauge(
    "harvester_last_run",
    "Last time the harvest was run (finished) in ms",
    registry=prom_registry,
)
prom_scanned_cubes = Gauge(
    "harvester_scanned_cubes",
    "Number of cubes scanned during the harvest",
    registry=prom_registry,
)
prom_updated_cubes = Gauge(
    "harvester_updated_cubes",
    "Number of cubes updated during the harvest",
    registry=prom_registry,
)
prom_errored_cubes = Gauge(
    "harvester_failed_updating_cubes",
    "Number of cubes that couldn't be updated due to a failure during the harvest",
    registry=prom_registry,
)


Base = declarative_base(metadata=MetaData(schema=config.db_schema))


class CubeMetadata(Base):
    __tablename__ = "metadata_cube"

    id = Column(Integer, primary_key=True)
    code = Column(String, nullable=False, unique=True)
    info = Column(String)
    last_update = Column(Date)

    dimensions = relationship("DimensionMetadata")

    def __repr__(self):
        return "<Cube(id='%s', code='%s', last_update='%s', ...)>" % (
            self.id,
            self.code,
            self.last_update,
        )


class DimensionMetadata(Base):
    __tablename__ = "metadata_dimension"

    id = Column(Integer, primary_key=True)
    cube_id = Column(Integer, ForeignKey("metadata_cube.id", ondelete="CASCADE"))
    code = Column(String, nullable=False, unique=True)
    info = Column(String)

    cube = relationship("CubeMetadata", back_populates="dimensions")
    elements = relationship("ElementMetadata")

    def __repr__(self):
        return "<Dimension(id='%s', code='%s', cube='%s', ...)>" % (
            self.id,
            self.code,
            self.cube.code,
        )


class ElementMetadata(Base):
    __tablename__ = "metadata_element"

    id = Column(Integer, primary_key=True)
    dimension_id = Column(
        Integer, ForeignKey("metadata_dimension.id", ondelete="CASCADE")
    )
    code = Column(String, nullable=False)
    info = Column(String)

    dimension = relationship("DimensionMetadata", back_populates="elements")

    __table_args__ = (
        UniqueConstraint("dimension_id", "code", name="metadata_element_uc"),
    )

    def __repr__(self):
        return "<Element(id='%s', code='%s', dimension='%s', ...)>" % (
            self.id,
            self.code,
            self.dimension.code,
        )

    def __lt__(self, other):
        return self.code < other.code


# see https://requests-cache.readthedocs.io/en/latest/user_guide.html#usage
def make_throttle_hook(timeout=1.0):
    def hook(response, *args, **kwargs):
        if not getattr(response, "from_cache", False):
            logger.debug("throttle hook: timeout: %s" % timeout)
            time.sleep(timeout)
        return response

    return hook


class SOHarvester:

    CUBE_DEFAULT_TIME_BACKLOG = datetime.date(1989, 1, 1)

    BASE_SO_API_URL = "https://data.statistics.sk/api/v2/"
    URL_COLLECTION_INFO = BASE_SO_API_URL + "collection"
    URL_DATASET_BASE = BASE_SO_API_URL + "dataset/"

    BASE_SO_BULK_URL = "https://data.statistics.sk/api/SendReport.php"

    DATA_TABLE_URL = "https://data.statistics.sk/api/getDataTable.php?out=data&language=sk&_=1684852392228"

    SO_JSON_VALUE_ID = "value"
    DB_COLUMN_VALUE = "value"

    # "be careful" stuff
    DIMENSION_ELEMENT_COUNT_LIMIT = 3

    RUN_REPORT = "harvesting complete; success: %d, errors: %d"

    def __init__(self, config):
        self.config = config

        requests_log = logging.getLogger("urllib3")
        requests_log.propagate = True

        self.filter_locations_re = None
        if config.filter_locations:
            self.filter_locations_re = re.compile(config.filter_locations)
        self.index_columns = []
        if config.index_columns:
            self.index_columns = config.index_columns.split(",")

        # HTTP requests stuff:
        self.requests_session = requests.Session()
        if config.be_careful:
            # to avoid hitting Stat. Office server too much,
            # we're going to cache all responses for configured amount of time
            self.requests_session = requests_cache.CachedSession(
                "so_cache", expire_after=config.cache_expire
            )
            self.requests_session.hooks = {
                "response": make_throttle_hook(config.throttle_sleep)
            }
        retry = Retry(connect=config.retry_connect, backoff_factor=config.retry_backoff)
        adapter = HTTPAdapter(max_retries=retry)
        self.requests_session.mount("https://", adapter)

        self.http_headers = None
        if config.contact_email is not None:
            adjusted_ua = "%s; %s" % (
                self.requests_session.headers["User-Agent"],
                config.contact_email,
            )
            self.http_headers = {
                "From": config.contact_email,
                "User-Agent": adjusted_ua,
            }

        # SQLAlchemy stuff:
        self.engine = create_engine(config.db_string, echo=config.sqlalchemy_echo)
        self.Session = sessionmaker(self.engine)
        self.session = self.Session()

        self.inspect = inspect(self.engine)
        self.mapper = registry()

        Base.metadata.create_all(self.engine, checkfirst=True)
        self.metadata = Base.metadata
        self.metadata.reflect(self.engine, schema=config.db_schema)

        # other stuff for collecting harvested data:
        self.collection_info = None
        self.cube_collection_info = {}
        self.cube_dataset_info = {}

        self.cubes = []
        self.dimensions = {}
        self.elements = {}

        self.cube_table = None

    @staticmethod
    def _derive_table_name(cm_code):
        return "cube_" + cm_code

    def _derive_full_table_name(self, cm_code):
        # full -> with schema included
        return self.config.db_schema + "." + SOHarvester._derive_table_name(cm_code)

    @staticmethod
    def _strip_cm_code_from_column_name(table_name, cm_code):
        """
        If we have a dimension 'om7011rr_obd' in cube 'om7011rr_obd', we'd like to use
        only 'obd' as row name.
        """

        return table_name.replace(cm_code + "_", "")

    def _is_time_dimension(self, cube_code, dm_code):
        """
        Determine whether given dimension is year.

        We need to treat years in some special way, say use 'Integer' type for them.

        Notes:
        - 'obdobie' = 'year'
        - 'rok' = 'year'
        """

        cdi = self._get_cube_dataset_info(cube_code)
        time_roles = cdi["role"]["time"]
        full_key = cube_code + "_" + dm_code
        return dm_code in time_roles or full_key in time_roles

    def _is_geo_dimension(self, cube_code, dm_code):
        """
        Determine whether given dimension represents some location
        since we may like to keep only some (say those relevant to PSK).

        Notes:
        - 'nuts14' = 'district'
        - 'nuts5' = "places" of various levels, from whole Slovakia down to municipalities
        - 'obc' -> 'obce' = 'municipalities'
        - 'okr' = 'disctrict'
        - 'vuc' = 'self-governing region'
        """

        cdi = self._get_cube_dataset_info(cube_code)
        if "geo" not in cdi["role"]:
            return False
        geo_roles = cdi["role"]["geo"]
        full_key = cube_code + "_" + dm_code
        return dm_code in geo_roles or full_key in geo_roles

    def _get_cube_metadata(self, cube_code):
        """
        Get our own metadata (our own = what we have in DB) about given cube.
        """

        cm = None
        try:
            cm = (
                self.session.query(CubeMetadata)
                .filter(CubeMetadata.code == cube_code)
                .one()
            )
        except NoResultFound:
            last_update = self.CUBE_DEFAULT_TIME_BACKLOG
            cm = CubeMetadata(code=cube_code, last_update=last_update)
            self.session.add(cm)

        return cm

    def _get_collection_info(self):
        if self.collection_info:
            return self.collection_info

        url = self.URL_COLLECTION_INFO
        self.collection_info = self.get_json(url, "fetching collection info")

        return self.collection_info

    def _get_cube_info(self, cube_code):
        """
        Since searching for a particular cube in collection info is slightly tricky,
        we're going to do it here and cache results.
        """

        if cube_code in self.cube_collection_info:
            return self.cube_collection_info[cube_code]

        ci = self._get_collection_info()
        index = 0
        for info in ci["link"]["item"]:
            if info["href"].startswith(self.URL_DATASET_BASE + cube_code):
                self.cube_collection_info[cube_code] = info
                logger.debug(
                    "cube dimension info for %s found at index %d" % (cube_code, index)
                )

                # parse date
                info["update"] = datetime.datetime.strptime(
                    info["update"], "%Y-%m-%d"
                ).date()

                return info
            index += 1

        raise KeyError("unknown cube: " + cube_code)

    def _get_cube_collection_info(self, cube_code):
        """
        According to SO API doc, this will give us info, from which we need among other things
        "last modification time" for given cube.
        """

        info = self._get_cube_info(cube_code)
        logger.debug("cube collection info: %s" % info)

        return info

    def _get_cube_dimensions(self, cube_code):
        """
        According to SO API doc, this will give us info, from which we need mainly
        list of dimensions for given cube.
        """

        info = self._get_cube_info(cube_code)
        dimensions = info["dimension"]
        logger.debug("dimensions: %s" % dimensions)

        return dimensions

    def _get_dimension_metadata(self, cm, dimension_code):
        """
        Get our own metadata (our own = what we have in DB) about given dimension.
        """

        dm = None
        try:
            dm = (
                self.session.query(DimensionMetadata)
                .filter(DimensionMetadata.code == dimension_code)
                .one()
            )
        except NoResultFound:
            dm = DimensionMetadata(code=dimension_code, cube_id=cm.id)
            self.session.add(dm)

        return dm

    def _get_dimension_elements(self, cube_code, dimension_code):
        """
        According to SO API doc, this will give us info, from which we need mainly
        list elements for given cube's dimension.
        """

        dimensions = self._get_cube_dimensions(cube_code)
        url = dimensions[dimension_code]["href"]
        elements = self.get_json(url, "fetching dimension elements")
        logger.debug("elements: %s" % elements)

        return elements

    def _get_element_metadata(self, dm, element_code):
        """
        Get our own metadata (our own = what we have in DB) about given element.
        """

        em = None
        try:
            em = (
                self.session.query(ElementMetadata)
                .filter(ElementMetadata.code == element_code)
                .filter(ElementMetadata.dimension_id == dm.id)
                .one()
            )
        except NoResultFound:
            em = ElementMetadata(code=element_code, dimension_id=dm.id)
            self.session.add(em)

        return em

    def process_element(self, cm, dm, element_code, elements):
        """
        Store in our metadata tables info about element belonging to given cube and dimension.
        """

        logger.debug(
            "processing element: %s (cube: %s, dimension: %s)"
            % (element_code, cm.code, dm.code)
        )
        em = self._get_element_metadata(dm, element_code)
        if "label" in elements:
            em.info = elements["label"][element_code]

        if not dm.code in self.elements:
            self.elements[dm.code] = []
        self.elements[dm.code].append(em)

        return em

    def process_dimension(self, cm, dimension_code, dimension_info):
        """
        Store in our metadata tables info about dimension belonging to given cube.
        """

        logger.debug("processing dimension: %s (cube: %s)" % (dimension_code, cm.code))
        dm = self._get_dimension_metadata(cm, dimension_code)
        dm.info = dimension_info["note"]

        elements_info = self._get_dimension_elements(cm.code, dm.code)
        element_info_list = elements_info["category"]
        elements = []
        for element_code in element_info_list["index"].keys():
            em = self.process_element(cm, dm, element_code, element_info_list)
            elements.append(em)
        logger.info("%d elements updated" % len(elements))

        if not cm.code in self.dimensions:
            self.dimensions[cm.code] = {}
        self.dimensions[cm.code][dimension_code] = dm

    def _get_cube_dataset_info(self, cube_code):
        """
        According to SO API doc, this will give us info, from which we need mainly
        list and order of arguments (dimensions) of dimensions for given cube.
        """

        if cube_code in self.cube_dataset_info:
            return self.cube_dataset_info[cube_code]

        info = self._get_cube_info(cube_code)
        url = info["href"]
        cube_info = self.get_json(url, "fetching cube dataset info")
        logger.debug("cube_collection_info: %s" % cube_info)

        self.cube_dataset_info[cube_code] = cube_info

        return cube_info

    def _create_cube_table_column_obj(self, cm, dm):
        """
        Create an SQLAlchemy Column object for given dimension.

        Main point here is to use proper type for given dimension/column.
        """

        column_name = self._strip_cm_code_from_column_name(dm.code.lower(), cm.code)
        index = column_name in self.index_columns
        if self._is_time_dimension(cm.code, dm.code):
            return Column(column_name, Integer, index=index)
        else:
            return Column(column_name, String, index=index)

    def _create_cube_table(self, cm):
        """
        Check whether given cube's table already exist, create if not.
        """

        if self.inspect.has_table(
            self._derive_table_name(cm.code), schema=self.config.db_schema
        ):
            return

        if not cm.code in self.dimensions:
            raise ValueError("missing dimensions for cube %s" % cm.code)
        dimensions = self.dimensions[cm.code]

        uc_name_list = []
        cube_table = Table(
            self._derive_table_name(cm.code),
            self.metadata,
            Column("id", Integer, primary_key=True),
        )

        dimension_names = self._get_dimension_names(cm)
        for dm_code in dimension_names:
            if not dm_code in dimensions:
                raise ValueError(
                    "missing dimension %s for cube %s" % (dm_code, cm.code)
                )
            dm = dimensions[dm_code]

            column = self._create_cube_table_column_obj(cm, dm)
            cube_table.append_column(column)
            uc_name_list.append(column.name)

        cube_table.append_column(Column(self.DB_COLUMN_VALUE, Float))

        if len(uc_name_list) > 0:
            uc_name = cube_table.name + "_uc"
            unique_constraint = UniqueConstraint(*uc_name_list, name=uc_name)
            cube_table.append_constraint(unique_constraint)

        # note: Maybe we can construct it even sooner (above) and while in the loop
        # creating Column objects we can also add fields to this class.
        data_class_name = "Cube" + cm.code[0].upper() + cm.code[1:]
        data_class = type(data_class_name, (object,), {})

        self.mapper.map_imperatively(data_class, cube_table)
        self.metadata.create_all(self.engine)
        logger.info("table %s created" % cm.code)

    # methods based on https://github.com/predicador37/pyjstat/blob/master/pyjstat/pyjstat.py
    # which is Apache 2.0 licensed, we're EUPL 1.2, which seems OK
    # (based on https://joinup.ec.europa.eu/collection/eupl/matrix-eupl-compatible-open-source-licences)
    # TODO: use pyjstat as requirement, may help simplify the code here
    def _get_dimension_index(self, data, name, value):
        """Get a dimension index from its name.
        Convert a dimension ID string and a category ID string into the
        numeric index of that category in that dimension
        Args:
           name(string): ID string of the dimension.
           value(string): ID string of the category.
        Returns:
           ndx[value](int): index of the category in the dimension.
        """
        if "index" not in data.get("dimension", {}).get(name, {}).get("category", {}):
            return 0
        ndx = data["dimension"][name]["category"]["index"]

        if isinstance(ndx, list):
            return ndx.index(value)
        else:
            return ndx[value]

    def _get_dimension_indices(self, data, query):
        """Get dimension indices.
        Converts a dimension/category list of dicts into a list of
        dimension indices.
        Args:
           query(list): dimension/category list of dicts.
        Returns:
           indices(list): list of dimensions' indices.
        """
        ids = data["id"] if data.get("id") else data["dimension"]["id"]
        indices = []

        for idx, ident in enumerate(ids):
            indices.append(
                self._get_dimension_index(
                    data, ident, [d.get(ident) for d in query if ident in d][0]
                )
            )

        return indices

    def _get_value_index(self, data, indices):
        """Convert a list of dimension indices into a numeric value index.
        Args:
            indices(list): list of dimension's indices.
        Returns:
           num(int): numeric value index.
        """
        size = data["size"] if data.get("size") else data["dimension"]["size"]
        ndims = len(size)
        mult = 1
        num = 0
        for idx, dim in enumerate(size):
            mult *= size[ndims - idx] if (idx > 0) else 1
            num += mult * indices[ndims - idx - 1]
        return num

    # /end of pyjstat.py

    def get_json(self, url, log_msg):
        logger.info("%s; url: %s" % (log_msg, url))
        r = self.requests_session.get(url, headers=self.http_headers)
        if r.status_code != 200:
            raise ValueError("failed to retrieve data: %s" % r.json())
        return r.json()

    def _get_stast_json_data_query_item(self, cm):
        """
        By the time we're in `get_and_store_cube_data()` we no longer work with dimension info which
        includes "ccc_data" (a.k.a. metric, with "ccc" being cube name). Hence we use this to get this
        info so that it can be used in the query for `_get_dimension_indices()`.
        """
        key = cm.code + "_data"
        value = list(
            self.cube_dataset_info[cm.code]["dimension"][key]["category"]["index"]
        )[0]
        return {key: value}

    def get_and_store_cube_data(self, cm, url, full_params, url_values, data=None):
        """
        According to SO API doc, this will give us actual data for given cube and dimensions.

        So we get here the data and push it to our database.
        """

        if url is None:
            raise ValueError("URL is None")

        if data is None:
            data = self.get_json(
                url, "requesting data from cube %s (via API)" % cm.code
            )
        else:
            logger.debug(
                "we already have data from cube %s using bulk download, skipping API call: %s"
                % (cm.code, url)
            )

        # store into DB
        data_obj = data_obj_upsert = {}
        stat_json_query = []
        stat_json_query_year_index = -1
        year_json_id = None
        year_column_name = None
        for index in range(len(full_params)):
            column_name = self._strip_cm_code_from_column_name(
                full_params[index], cm.code
            )
            if self._is_time_dimension(cm.code, full_params[index]):
                year_json_id = full_params[index]
                year_column_name = column_name
                stat_json_query.append({full_params[index]: None})
                stat_json_query_year_index = index
            else:
                data_obj[column_name] = url_values[index]
                stat_json_query.append({full_params[index]: url_values[index]})

        table_obj = self.metadata.tables[self._derive_full_table_name(cm.code)]

        data_years = data["dimension"][year_json_id]["category"]["index"]
        stat_json_query.append(self._get_stast_json_data_query_item(cm))
        for year in data_years.keys():
            data_obj[year_column_name] = year

            for key in stat_json_query[stat_json_query_year_index].keys():
                stat_json_query[stat_json_query_year_index][key] = year
            indices = self._get_dimension_indices(data, stat_json_query)
            value_index = self._get_value_index(data, indices)

            value = data["value"][value_index]
            # avoid having null where SO has value of zero but returns null in json
            if value is None:
                value = 0
            data_obj[self.DB_COLUMN_VALUE] = value

            insert_stmt = insert(table_obj).values(data_obj)
            do_update_stmt = insert_stmt.on_conflict_do_update(
                constraint=table_obj.name + "_uc", set_=data_obj_upsert
            )
            self.session.execute(do_update_stmt)

    def _filter_dimension_values_year(self, cm, elements):
        """
        Harvest only years which are newer then last update.
        """

        new_elements = []
        # -1 used because we have data for 2019 in 2020, etc.
        last_update_year = cm.last_update.year - 1
        for element in elements:
            if int(element.code) > last_update_year:
                new_elements.append(element)

        if len(new_elements) <= 0:
            logger.info(
                '"edited cube" detected: %s (edited = no new years, but cube has different'
                " last-modification time) => will re-fetch all years" % cm.code
            )
            return elements

        return new_elements

    def _filter_dimension_values_location(self, elements):
        """
        Harvest only "whole Slovakia", "generics" ("V", "M") and all concrete locations within PSK.
        """

        if self.filter_locations_re is None:
            if self.config.be_careful:
                logger.info(
                    "'filter_locations' not configured, harvesting all locations"
                )
            return elements

        new_elements = []
        for element in elements:
            if self.filter_locations_re.search(element.code):
                new_elements.append(element)

        return new_elements

    def _filter_dimension_values(self, cm, dimension_code, elements):
        """
        Some dimensions have too many values which we do not need all (say indicators for
        municipalities outside of PSK) or in regards to years we do not want to re-harvest
        years we've already got).

        So, here we are doing the "black magic" and guessing which is which and doing the filtering.
        """

        new_elements = elements
        if self._is_time_dimension(cm.code, dimension_code):
            new_elements = self._filter_dimension_values_year(cm, elements)
        elif self._is_geo_dimension(cm.code, dimension_code):
            new_elements = self._filter_dimension_values_location(elements)
        elif dimension_code.endswith("_dim1"):
            pass  # 6 items, nothing to filter
        elif dimension_code.endswith("_dim2"):
            pass  # 21 items, nothing to filter
        elif dimension_code.endswith("_ukaz"):
            pass  # 34 items, nothing to filter
        elif dimension_code.endswith("_vsk"):
            pass  # 37 items, nothing to filter
        elif dimension_code.endswith("_vek"):
            pass  # 113 items, nothing to filter
        elif len(elements) > self.DIMENSION_ELEMENT_COUNT_LIMIT:
            logger.warning(
                "there are %d elements in dimension %s, cube %s - consider some filtering"
                % (len(elements), dimension_code, cm.code)
            )
            if self.config.be_careful:
                logger.warning(
                    "forced shortening to %d items since code still not sufficiently good (disable with `be_careful = False`)"
                    % self.DIMENSION_ELEMENT_COUNT_LIMIT
                )
                new_elements = elements[: self.DIMENSION_ELEMENT_COUNT_LIMIT]

        if len(elements) != len(new_elements):
            logger.info(
                "%s dimension shortened from %d to %d items"
                % (dimension_code, len(elements), len(new_elements))
            )

        return new_elements

    def _get_dimension_names(self, cm):
        dimensions = self._get_cube_dimensions(cm.code)
        dimension_names = []
        for dimension in dimensions:
            dimension_names.append(dimension)

        return dimension_names

    def _get_dimension_values(self, cm, dimension_code):
        """
        This is a sub-task of '_construct_request()': for each dimension we need a list of dimension
        values (i.e. all the elements in the dimension) so that we can construct "fetch data URL"
        for all possible dimension values used in the cube, i.e. iterate over actual data.
        """

        dimension = self.dimensions[cm.code][dimension_code]
        elements = sorted(self.elements[dimension.code])
        elements = self._filter_dimension_values(cm, dimension_code, elements)
        return elements

    def _construct_request(
        self, cm, url, remaining_params, full_params, url_values, data=None
    ):
        """
        Construct all possible "fetch data URLs" for all possible dimension values used in the cube,
        to iterate over actual data and thus get it all.

        Beware, recursion: Each cube can have N dimensions (say between 2 and 5).
        We want to iterate through all values of a dimension and combine that into
        URLs, retrieve data from those and push that into DB.

        APV vs. bulk: If bulk is available, we have it in `data` and later on only get what we need from that.
        If API (e.g. "bulk not available"), `data` is None and later we will use API to get what we need.

        Example: We have dimension 'price' with values 'vat_excl' and 'with_vat' and
        we have dimension 'metric' with values 'with_vat' and 'average'.

        Thus we want to do following requests:
            1) /api/v2/dataset/<cube_id>/vat_excl/with_vat
            2) /api/v2/dataset/<cube_id>/vat_excl/average
            3) /api/v2/dataset/<cube_id>/with_vat/with_vat
            4) /api/v2/dataset/<cube_id>/with_vat/average
        """

        if len(remaining_params) <= 0:
            self.get_and_store_cube_data(cm, url, full_params, url_values, data)
            return

        if url is None:
            url = self.URL_DATASET_BASE + cm.code

        param = remaining_params[0]
        if self._is_time_dimension(cm.code, param):
            years = self._get_dimension_values(cm, param)  # sorted list ...
            if len(years) <= 0:
                # looks like we've run into "edited" cube, see https://gitlab.com/po-kraj-sk/so-harvester/issues/1
                raise ValueError(
                    'no new years found for cube %s - try "workaround" mentioned in https://gitlab.com/po-kraj-sk/so-harvester/issues/1#note_296686043'
                    % cm.code
                )
            elif len(years) == 1:
                # to use '.../2019/...' instead of '.../2019:2019/...' (= aesthetic + hopefully less CPU cycles at SO)
                param_value = years[0].code
            else:
                param_value = years[0].code + ":" + years[-1].code
            new_url = url + "/" + param_value
            new_url_values = url_values.copy()
            new_url_values.append(param_value)
            self._construct_request(
                cm, new_url, remaining_params[1:], full_params, new_url_values, data
            )
        else:
            for param_value in self._get_dimension_values(cm, param):
                new_url = url + "/" + param_value.code
                new_url_values = url_values.copy()
                new_url_values.append(param_value.code)
                self._construct_request(
                    cm, new_url, remaining_params[1:], full_params, new_url_values, data
                )

    def get_bulk_url(self, cm):
        for cube in self.cube_table["data"]:
            if cm.code != cube["code"]:
                continue
            # parsing HTML is hard(-er), hence lets (ab-)use observed pattern:
            url = "%s?cubeName=%s&lang=sk&fileType=json" % (
                self.BASE_SO_BULK_URL,
                cm.code,
            )
            if url not in cube["bulk"]:
                continue
            return url

        return None

    def etl_cube_data(self, cm):
        """High-level method which:
        1) makes sure we have DB table for given cube
        2) gets all the data from the cube and push it into our DB
        """

        self._create_cube_table(cm)

        bulk_url = self.get_bulk_url(cm)
        data = None
        if bulk_url is not None:
            # bulk vs. API:
            # a) with API we ask just for what we need, making several requests
            # b) with bulk file, we do just one request but get also years that we already have
            # TODO: For big cubes this may take too much run => use "streaming"
            # (https://github.com/ICRAR/ijson for example, not sure how that will go with pyjstat).
            data = self.get_json(
                bulk_url, "requesting data from cube %s (via bulk download)" % cm.code
            )

        params = self._get_dimension_names(cm)
        self._construct_request(cm, None, params, params, [], data)

    def process_cube(self, cube_code):
        """Checks whether given cube needs fetching/update and if so, run it.
        Returns a tuple (nb of updated cubes, nb of failures)
        """

        return_tuple = (1, 0)
        try:
            # check last modification time
            logger.info("processing cube: %s" % cube_code)
            cm = self._get_cube_metadata(cube_code)
            cci = self._get_cube_collection_info(cube_code)
            if cm.last_update >= cci["update"]:
                logger.info("cube %s is fresh, skipping update" % cube_code)
                # TODO: check whether truly needed
                self.session.rollback()
                return return_tuple

            # process metadata, create tables (if not present)
            dimension_info_list = self._get_cube_dimensions(cube_code)
            for dimension_code in dimension_info_list.keys():
                dimension_info = dimension_info_list[dimension_code]
                self.process_dimension(cm, dimension_code, dimension_info)
            self.session.commit()
            logger.info("%d dimensions updated" % len(self.dimensions[cm.code]))

            # get the data
            self.etl_cube_data(cm)
            self.session.commit()

            # all done, make a note into status info
            cm.last_update = cci["update"]
            cm.info = cci["label"]

            self.cubes.append(cm)
        except Exception as ex:
            logger.warning("error occurred: %s" % ex)
            logger.warning(traceback.format_exc())
            logger.warning("rolling back transaction for cube %s" % cube_code)
            self.session.rollback()
            return_tuple = (0, 1)
        finally:
            self.session.commit()

        return return_tuple

    def delete_cube(self, cube_code):
        # check:
        cm = None
        try:
            cm = (
                self.session.query(CubeMetadata)
                .filter(CubeMetadata.code == cube_code)
                .one()
            )
        except NoResultFound:
            logger.error("cube %s does not exist" % cube_code)

        # actual cube table
        table_name = self._derive_full_table_name(cube_code)
        table_obj = self.metadata.tables[table_name]
        table_obj.drop()
        logger.info("table %s dropped" % table_name)

        # metadata
        # TODO: After we add CASCADE, we need to "do it manually"
        dms = self.session.query(DimensionMetadata).filter(
            DimensionMetadata.cube_id == cm.id
        )
        count = 0
        for dm in dms:
            ems = self.session.query(ElementMetadata).filter(
                ElementMetadata.dimension_id == dm.id
            )
            count += ems.delete()
        logger.info("deleted %d element metadata entries" % count)
        count = dms.delete()
        logger.info("deleted %d dimension metadata entries" % count)
        count = (
            self.session.query(CubeMetadata)
            .filter(CubeMetadata.code == cube_code)
            .delete()
        )
        logger.info("deleted %d cube metadata entries" % count)

        self.session.commit()

    def get_cube_table(self):
        """
        Gets list of all cubes from the server. It is not part of /api/v2 but it is usable
        to determine whether bulk download (a.k.a. static file) is available for a cube.

        TODO: Get in touch with SO and discuss proper API call for that. Once where,
        among other things, HTML will not be embedded in JSON.
        """
        return self.get_json(self.DATA_TABLE_URL, "requesting cube table")

    @prom_harvest_time.time()
    def main(self):
        # command-line parameters
        parser = ArgumentParser()
        parser.add_argument("-d", "--delete", metavar="cube", help="a cube to delete")
        parser.add_argument(
            "-v",
            "--verbose",
            action="store_true",
            dest="verbose",
            default=False,
            help="be more verbose",
        )
        args = parser.parse_args()

        if args.verbose:
            logger.getLogger().setLevel("DEBUG")

        # handle cube removals
        if args.delete:
            self.delete_cube(args.delete)
            return

        # get basic cube info only once, since all cubes are there
        self.cube_table = self.get_cube_table()

        # list of cubes we're going to harvest
        cube_list = self.config.cube_list.split(",")

        successful_updates = 0
        failed_updates = 0
        for cube_code in cube_list:
            success, failure = self.process_cube(cube_code)
            successful_updates += success
            failed_updates += failure

        result_success = not (successful_updates == 0 and failed_updates > 0)
        if result_success:
            logger.info(self.RUN_REPORT % (successful_updates, failed_updates))
        else:
            logger.error(self.RUN_REPORT % (successful_updates, failed_updates))

        # Send metrics to prometheus pushgateway
        if self.config.prometheus_uri:
            try:
                prom_scanned_cubes.set(len(cube_list))
                prom_updated_cubes.set(successful_updates)
                prom_errored_cubes.set(failed_updates)
                prom_last_harvest_time.set(time.time() * 1000)
                logger.info("Pushing metrics to prometheus")
                push_to_gateway(
                    self.config.prometheus_uri,
                    job="so-harvester",
                    registry=prom_registry,
                )
                # push_to_gateway(self.config.prometheus_uri, job='health-harvester', registry=prom_registry)
            except Exception:
                logger.error(
                    "Unable to reach prometheus pushgateway {}".format(
                        self.config.prometheus_uri
                    )
                )

        return result_success


if __name__ == "__main__":
    so_harvester = SOHarvester(config)
    success = so_harvester.main()
    if not success:
        sys.exit(-1)
