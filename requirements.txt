psycopg[binary] >= 3.2.3, == 3.2.*
requests >= 2.32.3, == 2.32.*
requests-cache >= 1.2.1, == 1.2.*
SQLAlchemy >= 2.0.36, == 2.0.*
prometheus-client >= 0.21.1, == 0.21.*
