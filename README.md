# Intro

In order to be nice to Statistical Office, i.e. use data from them responsibly,
this script harvests selected data cubes into local PostgreSQL database.

On first run it fetches all the data, on subsequent runs only updates.
Thus it is supposed to be run periodically, from the `cron`,
once a week or at most once a day (since source data is usually updated
annually), preferably during the night.

What does it mean "responsibly": While harvesting does make some load
on the server(s) of Statistical Office, when performing more complicated
analytics or programming apps, it is best to have local copy of the data
to avoid repeated requests for same data. Otherwise load from analytics
or apps on Statistical Office may get unbearable and cause disruption
of their service,


# License

This code is licensed under the [EUPL](LICENSE.txt).


# Requirements

Python 3.11 or newer


# How to install

1. checkout from Git: https://gitlab.com/po-kraj-sk/so-harvester
2. create virtual environment: `virtualenv --python=python3.11 .venv`
3. activate: `source .venv/bin/activate`
4. install dependencies: `pip install -r requirements.txt`
5. create configuration file: `cp so_harvester.conf.example so_harvester.conf`

Then also check:

6. that database exists along with schema 'statsoffice'
7. user running harvester is able to create tables in the database schema
8. configuration in `so_harvester.conf`, adjust if necessary


# How to run

`python so_harvester.py`


## Typical configuration changes

`so_harvester.conf.example` contains setup which works for people from PSK
analytic and GIS units who are interested in certain data about Presov
region. So given different needs of different users, following needs to be
adjusted for others:


### cube_list

Remove or add cubes as needed. Consult
(API page of Statistical Office)[https://data.statistics.sk/api/]
or some other pages to obtain information about cubes and their contents.


### filter_locations

Example configuration is limiting data harvesting to:

- `SK041.+`: all NUTS codes under Presov region, including Presov region
  (SK041)
- `M`: special NUTS code in some cubes, represents "City",
  e.g. value representing cities in a certain area
- `V`: special NUTS code in some cubes, represents "Country"
  e.g. value representing villages/country in a certain area
- `SK0`: whole area of Slovak Republic
- `SK04`: Eastern Slovakia region

With this set, PSK database contains all detailed data for Presov region and
only high-level data for other regions and whole country, thus avoiding the need
to fetch and store detailed data about whole country.

Reference: https://sk.wikipedia.org/wiki/SK-NUTS


### index_columns

Since indexes are a trade-off between speed and disk space, you should
remove indexes you do not need and add indexes which help to make your
queries quicker.

Having too many indexes also slows down harvesting.


### be_careful setting

When `True`, harvester will try avoiding overloading either source server
or target database.

Following will be done to avoid overloading source server:

1. server responses will be kept in local SQlite cache
2. after getting data from server, small sleep will be done

Then, to prevent unexpected load on target database,
following will be also done:

3. when unknown dimension with many elements is encountered,
   only some of the elements will be actually fetched


## Updates of cube schema

As of now, code does not handle changes in dimension elements list
(i.e. DB schema updates).

If cube schema changes, DB (or affected tables) needs to be dropped
and all data downloaded again.

Or DB schema (table names and structure) needs to be updated by hand.


# Using Docker
A Dockerfile and a docker-compose are available. The docker-compose provides an
easy way to build and test the docker image and the harvesting service.

To see the harvester in action:
    # build the image
    docker-compose build
    # run it
    docker-compose up -d
    # follow the logs
    docker-compose logs -f

You can see the results in the associated database, on port 5433 (use your
favorite tool for database inspection).

The docker-compose.yml file shows you how to configure the docker container
using environment variables. The list of available environment variables can be
seen in the Dockerfile.

*One specific case to remark: LOCATIONS_REGEX_FILTER has an `$` character in it.
 This is considered by docker-compose as a variable substitution symbol, so it
 had to be escaped by doubling it, hence the `$$` at the end of it.*

## Logs persistence
You are expected to bind a volume to `/app/logs` folder. If bind-mounting to a FS folder, the chown command from the Dockerfile will not  operate: you will have to manually create and chown the volume folder to uid 999

# Information for developers

Branches, pull-requests, releases, etc.: according to [git-flow](http://danielkummer.github.io/git-flow-cheatsheet/) but ...

... in order to allow better overview for current and future contributors, we'd like to conclude features with pull-requests,
i.e. instead of `git flow feature finish MYFEATURE` do following:

1. `git flow feature publish MYFEATURE`
2. go to GitLab and open pull-request from your feature branch to `develop`
3. review + adjustments
4. merge + delete branch after merge


## Git hooks

For source code formatting, etc.:

`pre-commit install`


# About Statistical Office API

For more information about the API please visit:

https://data.statistics.sk/api/

Using that, you can either download whole cubes in bulk or define parameters
based on which that service generates direct URL for bulk download of a part
of a cube.

Since some cubes are too big to fetch via one download, that second feature
is crucial. (And that's essentially base of what this harvester is doing:
using such URLs to fetch portions of a cube, it maintains copy of a cube
or part of the cube in your local PostgreSQL database.)
