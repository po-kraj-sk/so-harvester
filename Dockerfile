FROM python:3.11

LABEL project="georchestra.org"
LABEL org.opencontainers.image.authors="jeanpommier@pi-geosolutions.fr"

#install envsubst
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        gettext-base \
        ncat \
    && rm -rf /var/lib/apt/lists/*


# Add non-privileged user to run the script
RUN useradd -M -s /bin/bash -U harvester -u 999


# Copy the application
COPY --chown=harvester:harvester requirements.txt logging_config* so_harvester* LICENSE.txt README.md /app/
#COPY  requirements.txt so_harvester* LICENSE.txt README.md /app/
RUN pip install -r /app/requirements.txt
WORKDIR /app


# Set up entrypoint. Will take care of the app's configuration
COPY --chown=harvester:harvester docker /
#COPY docker /
RUN chmod +x /entrypoint.sh &&\
    chmod +x /docker-entrypoint.d/*
ENTRYPOINT [ "/entrypoint.sh" ]


ENV CUBES_LIST="cr3002rr,np3110rr,og3010rr,og3011rr,om7009rr,om7010rr,om7011rr,om7038rr,pr3113rr,pr3113rr,st3006rr,zd3002rr" \
    LOCATIONS_REGEX_FILTER="^(SK041.+|M|V|SK0|SK04)$" \
    INDEX_COLUMNS="rok,obd" \
    DB_CONNECTION_STRING="" \
    DB_CONNECTION_STRING_FILE="" \
    DB_SCHEMA="statsoffice" \
    CONTACT_EMAIL="so_harvester@unknown.org" \
    LOG_LEVEL="INFO" \
    RETRY_CONNECT="3" \
    RETRY_BACKOFF="0.25" \
    BE_CAREFUL="True" \
    DEBUG="false" \
    LOG_FILE="/app/logs/harvesters/so_harvester.log"

RUN mkdir -p /app/logs && chown harvester:harvester /app/logs
VOLUME /app/logs

# Run as Alpine's Guest user
USER harvester
CMD ["python", "/app/so_harvester.py"]
