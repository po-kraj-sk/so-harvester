# configuration for harvester in Docker image

[so_harvester]
cube_list = $CUBES_LIST
filter_locations = $LOCATIONS_REGEX_FILTER
index_columns=$INDEX_COLUMNS

db = postgresql+psycopg://$DB_CONNECTION_STRING
db_schema = $DB_SCHEMA

be_careful = $BE_CAREFUL
# 1 day, in seconds
cache_expire = 86400
throttle_sleep=0.25

contact_email=$CONTACT_EMAIL

[sqlalchemy]
echo = False

[requests]
retry_connect=$RETRY_CONNECT
retry_backoff=$RETRY_BACKOFF

[prometheus]
uri = %(PROM_PUSHGATEWAY_URI)s
